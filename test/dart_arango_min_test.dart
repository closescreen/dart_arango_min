import 'package:dart_arango_min/dart_arango_min.dart';
import 'package:test/test.dart';

import 'test_conf.dart';

void main() {
  group('Client can:', () {
    final sch = dbscheme;
    final h = dbhost;
    final p = dbport;
    const systemDb = '_system';
    const testDb = 'test_temp_db';
    const testCollection = 'test_temp_collection';
    String testDocumentKey = '';
    String testDocumentRev = '';
    var testMultipleDocumentsKeys = <String?>[];
    final u = dbuser;
    final ps = dbpass;
    const realm = '';

    var clientSystemDb = ArangoDBClient(
        scheme: sch,
        host: h,
        port: p,
        db: systemDb,
        user: u,
        pass: ps,
        realm: realm);

    test('Get current db info', () async {
      var answer = await clientSystemDb.currentDatabase();
      expect(answer, contains('error'));
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer, contains('result'));
      expect(answer['result'] as Map?, contains('name'));
      expect(answer['result']['name'] as String?, equals(systemDb));
      expect(answer['result'] as Map?, contains('isSystem'));
      expect(answer['result']['isSystem'] as bool?, equals(true));
    });

    test('List of accessible databases', () async {
      var answer = await clientSystemDb.userDatabases();
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'] as bool?, equals(false));
      expect(answer, contains('result'));
      expect(answer['result'] as List?, contains(systemDb));
    });

    test('List of all existing databases', () async {
      var answer = await clientSystemDb.existingDatabases();
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'] as bool?, equals(false));
      expect(answer, contains('result'));
      expect(answer['result'] as List?, contains(systemDb));
    });

    test('Create a database', () async {
      // first, ask about all databases
      var databases = await clientSystemDb.existingDatabases();
      // skip test if test database already exists
      if (databases.containsKey('result') &&
          (databases['result'] as List).contains(testDb)) {
        print(
            // ignore: lines_longer_than_80_chars
            'Skip test for creating database because database ${testDb} already exists.');
        return;
      }
      // creating the test database
      var answer = await clientSystemDb.createDatabase({
        'name': testDb,
        'users': [
          {
            'username': u,
            'passwd': ps,
          },
        ],
      });
      expect(answer, contains('error'));
      expect(answer['error'] as bool?, equals(false));
      expect(answer, contains('result'));
      expect(answer['result'], equals(true));
    });

    // changing current database
    var testDbClient = ArangoDBClient(
        scheme: sch,
        host: h,
        port: p,
        db: testDb,
        user: u,
        pass: ps,
        realm: realm);

    test('create collection', () async {
      var allCollectionsAnsw = await testDbClient.allCollections();
      var alreadyExists = (allCollectionsAnsw['result'] as List)
          .any((coll) => coll['name'] == testCollection);
      if (alreadyExists) {
        print(
            'Skip creating collection ${testCollection} bexause it is exists');
      } else {
        var answer =
            await testDbClient.createCollection({'name': testCollection});
        if (answer['error'] == true) {
          print(answer);
        }
        expect(answer['error'], false);
        expect(answer['name'] as String?, testCollection);
      }
    });

    test('truncate collection', () async {
      var answer = await testDbClient.truncateCollection(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer['name'] as String?, testCollection);
    });

    test('get collection info', () async {
      var answer = await testDbClient.collectionInfo(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer['name'] as String?, testCollection);
      expect(answer, contains('status'));
    });

    test('get collection properties', () async {
      var answer = await testDbClient.collectionProperties(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('waitForSync'));
      expect(answer, contains('keyOptions'));
    });

    test('get count of documents in collection', () async {
      var answer = await testDbClient.documentsCount(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('count'));
    });

    test('get statistics for a collection', () async {
      var answer = await testDbClient.collectionStatistics(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('figures'));
    });

    test('get collection revision id', () async {
      var answer = await testDbClient.collectionRevisionId(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('revision'));
    });

    test('get collection checksum', () async {
      var answer = await testDbClient.collectionChecksum(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('checksum'));
    });

    test('get all collections', () async {
      var answer = await testDbClient.allCollections();
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'], false);
      expect(answer, contains('result'));
    });

    test('create document', () async {
      var answer = await testDbClient
          .createDocument(testCollection, {'Hello': 'World'});
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer,
          allOf(contains('_id'), contains('_key'), contains('_rev')));
      // save document key for next test:
      testDocumentKey = answer['_key'] ?? '';
    });
    test('getDocumentByKey returns Map with _key', () async {
      var answer = await testDbClient.getDocumentByKey(
          testCollection, testDocumentKey);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer, contains('_key'));
    });

    test('getDocumentByKey can require document revision', () async {
      // first get the doc:
      var answer = await testDbClient.getDocumentByKey(
          testCollection, testDocumentKey);
      // get its revision:
      expect(answer, contains('_rev'));
      if (answer['error'] == true) {
        print(answer);
      }
      // save its revision:
      testDocumentRev = answer['_rev'] ?? '';

      // now try to get the document with not matched revision:
      var emptyAnswer = await testDbClient.getDocumentByKey(
          testCollection, testDocumentKey,
          ifNoneMatchRevision: testDocumentRev);

      // because last revision equals ${testDocumentRev} 
      // server answer will empty:
      expect(emptyAnswer, equals({}));

      // get document only required revision:
      var answerWithRevision = await testDbClient.getDocumentByKey(
          testCollection, testDocumentKey,
          ifMatchRevision: testDocumentRev);

      expect(answerWithRevision, contains('_rev'));
      if (answerWithRevision['error'] == true) {
        print(answerWithRevision);
      }
      expect(answerWithRevision['_rev'], equals(testDocumentRev));

      // try get doc with noexists revision:
      var notExistAnswer = await testDbClient.getDocumentByKey(
          testCollection, testDocumentKey,
          ifMatchRevision: 'my_wrong_rev');

      // we will get error responce:
      expect(notExistAnswer, contains('error'));
      expect(notExistAnswer['error'], equals(true));
      expect(notExistAnswer, contains('code'));
      expect(notExistAnswer['code'], equals(412));
    });

    test('update document', () async {
      // update document with returnNew:
      var updateAnswer = await testDbClient.updateDocument(
          testCollection, testDocumentKey, {'Hello': 'University'},
          queryParams: {'returnNew': 'true'});

      if (updateAnswer['new']['Hello'] != 'University') print(updateAnswer);

      expect(updateAnswer['new']['Hello'], 'University');

      // save revision:
      testDocumentRev = updateAnswer['_rev'] ?? '';

      var matchedUpdateAnswer = await testDbClient.updateDocument(
          testCollection, testDocumentKey, {'Hello': 'Underground'},
          ifMatchRevision: testDocumentRev);

      // document was updated:
      expect(matchedUpdateAnswer, contains('_oldRev'));
      expect(matchedUpdateAnswer['_oldRev'], equals(testDocumentRev));

      // try to update not matched revision:
      var notMatchedUpdateAnswer = await testDbClient.updateDocument(
          testCollection,
          testDocumentKey,
          {'Bad trying': 'because bad revision'},
          ifMatchRevision: 'my_bad_rev');

      // we will get error in answer:
      expect(notMatchedUpdateAnswer, contains('error'));
      expect(notMatchedUpdateAnswer['error'], equals(true));
      expect(notMatchedUpdateAnswer['code'], equals(412));
    });

    test('replace document', () async {
      // replace document with returnNew:
      var replaceAnswer = await testDbClient.replaceDocument(
          testCollection, testDocumentKey, {'Goodby': 'Moon'},
          queryParams: {'returnNew': 'true'});

      if (replaceAnswer['new']['Goodby'] != 'Moon') print(replaceAnswer);

      expect(replaceAnswer['new']['Goodby'], 'Moon');

      // save revision:
      testDocumentRev = replaceAnswer['_rev'] ?? '';

      var matchedReplaceAnswer = await testDbClient.replaceDocument(
          testCollection, testDocumentKey, {'Hello': 'Undeground'},
          ifMatchRevision: testDocumentRev);

      // document was updated:
      expect(matchedReplaceAnswer, contains('_oldRev'));
      expect(matchedReplaceAnswer['_oldRev'], equals(testDocumentRev));

      // try to update not matched revision:
      var notMatchedReplaceAnswer = await testDbClient.replaceDocument(
          testCollection,
          testDocumentKey,
          {'Bad trying': 'because bad revision'},
          ifMatchRevision: 'my_bad_rev');

      // we will get error in answer:
      expect(notMatchedReplaceAnswer, contains('error'));
      expect(notMatchedReplaceAnswer['error'], equals(true));
      expect(notMatchedReplaceAnswer['code'], equals(412));
    });

  test('replace multiple documents', () async {
      var replaceAnswer = await testDbClient.replaceDocuments(
          testCollection, [{'_key':testDocumentKey, 'Good evening': 'Jupiter'}],
          queryParams: {'returnNew': 'true'});

      if (replaceAnswer[0]['new']['_key'] != testDocumentKey){
        print(replaceAnswer);
      }

      expect(replaceAnswer[0]['new']['_key'], testDocumentKey);

    });

    test('remove document', () async {
      var answer = await testDbClient
          .removeDocument(
            testCollection, 
            testDocumentKey, 
            queryParams: {'returnOld' : 'true'} 
      ) as Map;

      if (answer['error'] == true) {
        print(answer);
      }

      expect(answer,
          allOf(contains('_id'), contains('_key'), contains('_rev')));

      if (!answer.containsKey('old')) print(answer);
      
      if (answer['old']['Good evening'] != 'Jupiter') print(answer);

      expect(answer['old']['Good evening'], 'Jupiter');
      
      // After this test document with `testDocumentKey` should be deleted.
    });

    test('create multiple documents', () async {
      var answer = await testDbClient.createDocument(testCollection, [
        {'Hello': 'Earth'},
        {'Hello': 'Venus'}
      ]);
      // 2 documents was inserted:
      expect((answer as List).length, equals(2));
      // about first document:
      expect(answer[0],
          allOf(contains('_id'), contains('_key'), contains('_rev')));
      
      // save documents keys for later tests:
      for (var doc in answer) {
        testMultipleDocumentsKeys.add(doc['_key']);
      }
    });


    test('remove multiple documents', () async {
      var answer = await testDbClient.removeDocuments(testCollection, [
        // data should contain list of map with _key or _id attributes 
        // for each document to remove
      {
        '_key': testMultipleDocumentsKeys[0],
      },
      {
        '_key': testMultipleDocumentsKeys[1],
      },]);

      // print('----------> ${answer}');
      expect((answer as List).length, equals(2));

      // first of removed documents has the same key as in request body:
      expect(answer[0]['_key'], equals(testMultipleDocumentsKeys[0]));

    });

    test('drop collection', () async {
      var answer = await testDbClient.dropCollection(testCollection);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer['error'] as bool?, false);
    });

    // back to _system db
    test('drop test database', () async {
      var answer = await clientSystemDb.dropDatabase(testDb);
      if (answer['error'] == true) {
        print(answer);
      }
      expect(answer, contains('result'));
      expect(answer['result'], equals(true));
    });

  });
}
