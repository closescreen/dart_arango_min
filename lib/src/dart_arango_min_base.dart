// Put public facing types in this file.

import 'dart:async';
import 'dart:convert';
import 'dart:io';

class ArangoDBClient {
  final String scheme;
  final String host;
  final int port;
  final String db;
  final String? user;
  final String? pass;
  final String realm;

  late Uri dbUrl;
  late HttpClientBasicCredentials credentials;
  late HttpClient httpClient;

  ArangoDBClient({
    this.scheme = 'http',
    this.host = 'localhost',
    this.port = 8529,
    this.db = '_system',
    this.user,
    this.pass,
    this.realm = '',
  }) {

    ({
      'sheme': scheme,
      'db':db,
      'host':host,
      'port':port,
      'user':user,
      'pass':pass,
      'realm':realm,
      }).forEach((k,v)=>ArgumentError.checkNotNull(v,k));

    dbUrl =
        Uri(scheme: scheme, host: host, port: port, pathSegments: ['_db', db]);

    _connect();
  }

  void _connect() {
    credentials = HttpClientBasicCredentials(user!, pass!);
    httpClient = HttpClient();
    httpClient.addCredentials(dbUrl, realm, credentials);
  }

  /// Retrieves information about the current database
  /// https://www.arangodb.com/docs/devel/http/database-database-management.html#information-of-the-database
  Future currentDatabase() async =>
      _httpGet(['_db', db, '_api', 'database', 'current']);

  /// List of accessible databases for current user
  /// https://www.arangodb.com/docs/devel/http/database-database-management.html#list-of-accessible-databases
  Future userDatabases() async =>
      _httpGet(['_db', db, '_api', 'database', 'user']);

  /// Retrieves the list of all existing databases (availabe or not)
  /// https://www.arangodb.com/docs/devel/http/database-database-management.html#list-of-databases
  Future existingDatabases() async =>
      _httpGet(['_db', db, '_api', 'database']);

  /// Creates a database.
  /// The data with these properties is required:
  ///
  /// name: Has to contain a valid database name.
  /// users: Has to be an array of user objects to
  ///  initially create for the new database.
  ///
  /// User information will not be changed for users that already exist.
  ///   If users is not specified or does not contain any users,
  ///   a default user root will be created with an empty string password.
  ///   This ensures that the new database will be
  ///  accessible after it is created.
  ///   Each user object can contain the following attributes:
  ///
  /// username: Login name of the user to be created
  /// passwd: The user password as a string. 
  /// If not specified, it will default to an empty string.
  /// active: A flag indicating whether the user account 
  /// should be activated or not.
  ///   The default value is true. 
  /// If set to false, the user won’t be able to log into the database.
  /// extra: A JSON object with extra user information.
  ///   The data contained in extra will be stored 
  /// for the user but not be interpreted further by ArangoDB.
  /// https://www.arangodb.com/docs/devel/http/database-database-management.html#create-database
  Future createDatabase(Map<String, Object> data) async =>
      _httpPost(['_db', db, '_api', 'database'], data);

  /// Drops the database along with all data stored in it.
  /// https://www.arangodb.com/docs/devel/http/database-database-management.html#drop-database
  Future dropDatabase(String name) async =>
      _httpDelete(['_db', db, '_api', 'database', name]);

  /// Creates a collection
  /// See https://www.arangodb.com/docs/3.4/http/collection-creating.html for details.
  Future createCollection(Map<String, Object> data) async =>
      _httpPost(['_db', db, '_api', 'collection'], data);

  /// Drops a collection
  /// https://www.arangodb.com/docs/stable/http/collection-creating.html#drops-a-collection
  Future dropCollection(String name, {bool isSystem = false}) async =>
      _httpDelete(['_db', db, '_api', 'collection', name],
          queryParameters: {'isSystem': jsonEncode(isSystem)});

  /// Truncates a collection
  Future truncateCollection(String name) async =>
      _httpPut(['_db', db, '_api', 'collection', name, 'truncate']);

  /// Gets id, name, status of collection
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#return-information-about-a-collection
  Future collectionInfo(String name) async =>
      _httpGet(['_db', db, '_api', 'collection', name]);

  /// Get collection properties
  /// See https://www.arangodb.com/docs/3.4/http/collection-getting.html#read-properties-of-a-collection for details
  Future collectionProperties(String name) async =>
      _httpGet(['_db', db, '_api', 'collection', name, 'properties']);

  /// Returns number of documents in a collection
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#return-number-of-documents-in-a-collection
  Future documentsCount(String collectionName) async =>
      _httpGet(['_db', db, '_api', 'collection', collectionName, 'count']);

  /// Returns statistics for a collection
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#return-statistics-for-a-collection
  Future collectionStatistics(String collectionName) async =>
      _httpGet(['_db', db, '_api', 'collection', collectionName, 'figures']);

  /// Returns collection revision id
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#return-collection-revision-id
  Future collectionRevisionId(String collectionName) async =>
      _httpGet(['_db', db, '_api', 'collection', collectionName, 'revision']);

  /// Returns checksum for the collection
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#return-checksum-for-the-collection
  Future collectionChecksum(String collectionName) async =>
      _httpGet(['_db', db, '_api', 'collection', collectionName, 'checksum']);

  /// Reads all collections
  /// https://www.arangodb.com/docs/3.4/http/collection-getting.html#reads-all-collections
  Future allCollections() async => _httpGet(['_db', db, '_api', 'collection']);

  /// Creates document
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#create-document
  Future createDocument(String collectionName, data) async =>
      _httpPost(['_db', db, '_api', 'document', collectionName], data);

  /// Reads a single document
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#read-document
  ///
  /// If ifNoneMatchRevision was given
  /// and this revision equals the latest version of found document, 
  /// then server will return 304 status code
  /// and this driver will return empty Map.
  ///
  /// If ifMatchRevision was given
  /// and document with this revision was not found,
  /// then not empty Map will returned, 
  /// but with code=412 and error=true and corresponding errorMessage.
  Future getDocumentByKey(String collection, String documentKey,
          {String? ifNoneMatchRevision, String? ifMatchRevision}) async =>
      _httpGet([
        '_db',
        db,
        '_api',
        'document',
        collection,
        documentKey
      ], headers: {
        'accept': 'application/json',
        'If-None-Match': ifNoneMatchRevision,
        'If-Match': ifMatchRevision,
      });

  /// Replaces document
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#replace-document
  Future replaceDocument(
    String collectionName, String documentKey, Map<String, dynamic> data,
    {String? ifMatchRevision, Map<String, dynamic>? queryParams}
  ) async => 
    await _httpPut( 
      ['_db',db,'_api','document',collectionName, documentKey], 
      postData: data,
      queryParameters: queryParams,
      headers: {'If-Match': ifMatchRevision} );



  /// Replaces multiple documents
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#replace-documents
  Future replaceDocuments(
    String collectionName, List<Map<String, dynamic>> data,
    { Map<String, dynamic>? queryParams}
  ) async => 
    await _httpPut( 
      ['_db',db,'_api','document',collectionName], 
      postData: data,
      queryParameters: queryParams,
  );


  /// Updates a document
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#update-document
  Future updateDocument(
          String collectionName, String documentKey, Map<String, dynamic> data,
          {String? ifMatchRevision, Map<String, dynamic>? queryParams}) async =>
      _httpPatch(
          ['_db', db, '_api', 'document', collectionName, documentKey], data,
          queryParameters: queryParams,
          headers: {'If-Match': ifMatchRevision});

  // td: updates multiple documents
  // https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#update-documents

  /// Removes a document
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#removes-a-document
  Future removeDocument(
          String collectionName, String documentKey,
          {String? ifMatchRevision, Map<String, dynamic>? queryParams}) async =>
      _httpDelete(
          ['_db', db, '_api', 'document', collectionName, documentKey],
          queryParameters: queryParams,
          headers: {'If-Match': ifMatchRevision});

  /// td: Removes multiple documents
  /// https://www.arangodb.com/docs/3.4/http/document-working-with-documents.html#removes-multiple-documents
  Future removeDocuments(
          String collectionName, List<Map<String, dynamic>> data,
          { Map<String, dynamic>? queryParams}) async =>
      _httpDelete(
          ['_db', db, '_api', 'document', collectionName],
          postData: data,
          queryParameters: queryParams,
          );

  Future _toMap(HttpClientRequest req) async {
    var resp = await req.close();
    var text = await resp.transform(utf8.decoder).join();

    if (text.isEmpty) {
      return {};
    }
    try {
      return json.decode(text);
    } catch (e) {
      throw {
        'message': "Can't decode HTTP response as json",
        'error': e,
        'response text': text,
        //  "request": await _toMap(req),
      };
    }
  }

  Future queryFirstBatch(Map<String, Object> data) async =>
      _httpPost(['_db', db, '_api', 'cursor'], data);

  Future fetchNextBatch(int cursorId) async =>
      _httpPut(['_db', db, '_api', 'cursor', cursorId.toString()]);

  /// Makes query to ArangoDB batabase.
  Stream queryToStream(Map<String, Object> data) async* {
    Map resultData = await (queryFirstBatch(data));
    yield resultData;
    while (resultData.containsKey('id') &&
        resultData.containsKey('hasMore') &&
        resultData['hasMore'] != null &&
        resultData['hasMore'] as bool == true) {
      var cursorId = int.parse(resultData['id'] as String);
      resultData = await (fetchNextBatch(cursorId));
      yield resultData;
    }
  }

  /// Makes query to ArangoDB batabase, 
  /// collect results in memory and return as List.
  /// If result has error - throws error.
  Future<List> queryToList(Map<String, dynamic> data) async {
    var result = [];
    await for (var batch in queryToStream(data as Map<String, Object>)) {
      if ( batch['error'] ){
        throw batch;
      }
      var records = batch['result'] as List? ?? [];
      for (var record in records) {
        result.add(record);
      }
    }
    return result;
  }

  Future _httpGet(Iterable<String> pathSegments,
          {Map<String, dynamic>? queryParameters,
          Map<String, dynamic>? headers}) async =>
      _http('GET', pathSegments,
          queryParameters: queryParameters, headers: headers);

  Future _httpDelete(Iterable<String> pathSegments,
          { postData,
            Map<String, dynamic>? queryParameters,
            Map<String, dynamic>? headers
          }) async =>
      _http('DELETE', pathSegments,
          queryParameters: queryParameters, 
          headers: headers, 
          postData: postData
      );

  Future _httpPut(Iterable<String> pathSegments,
          { postData,
            Map<String, dynamic>? queryParameters,
            Map<String, dynamic>? headers
          }) async =>
      _http('PUT', pathSegments,
          queryParameters: queryParameters, 
          headers: headers, 
          postData:  postData);

  Future _httpPost(Iterable<String> pathSegments, postData,
          {Map<String, dynamic>? queryParameters,
          Map<String, dynamic>? headers}) async =>
      await _http('POST', pathSegments,
          queryParameters: queryParameters,
          headers: headers,
          postData: postData);

  Future _httpPatch(Iterable<String> pathSegments, postData,
          {Map<String, dynamic>? queryParameters,
          Map<String, dynamic>? headers}) async =>
      await _http('PATCH', pathSegments,
          queryParameters: queryParameters,
          headers: headers,
          postData: postData);

  Future _http(
    String method,
    Iterable<String> pathSegments, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    postData,
  }) async {
    var url = dbUrl.replace(pathSegments: pathSegments as Iterable<String>);

    if (queryParameters != null) {
      url = url.replace(queryParameters: queryParameters);
    }

    var req = await httpClient.openUrl(method, url);

    if (headers != null) {
      for (var headerName in headers.keys) {
        if (headers[headerName] == null) continue;
        req.headers.add(headerName, headers[headerName]);
      }
    }

    if (postData != null) {
      var json = jsonEncode(postData);
      req.headers.contentType = ContentType.json;
      req.headers.contentLength = utf8.encode(json).length;
      req.write(json);
    }

    return await _toMap(req);
  }
}
