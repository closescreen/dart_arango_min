abstract class QueryTextFragment {
  String? line;
  @override
  String toString() => line!;
}

/// Simple text line for injecting it into the query, constructed by [Query()].
/// Sample:
///  ```
/// Query([ Line('FOR doc in documents'), ... ])
/// ```
class Line extends QueryTextFragment {
  @override
  String? line;

  Line(this.line);

  @override
  String toString() => line!;
}

/// Line of query text with condition.
/// If [bool] `cond` is true, then [String] `when_true` will pasted into query.
/// Sample:
/// ```
/// Query([
///   Line('FOR doc in documents'),
///   LineIfThen(tag != null, 'FILTER doc.tags && POSITION( doc.tags, tag )'),
///   ...
/// ])
/// ```
class LineIfThen extends QueryTextFragment {
  LineIfThen(bool cond, String whenTrue) {
    line = cond ? whenTrue : '';
  }
}

/// Line of query text with condition.
/// If [bool] `cond` is true, then [String] `when_true` will pasted into query.
/// Else [String] `when_false` willpasted into query.
/// Sample:
/// ```
/// Query([
///   Line('FOR doc in documents'),
///   LineIfThen(tag != null, 'FILTER doc.tags && POSITION( doc.tags, tag )'),
///   Line('SORT doc.datetime'),
///   LineIfThenElse(
///     without_content, 'return UNSET( doc, "content" )', 'return doc'),
///   ...
/// ])
/// ```
class LineIfThenElse extends QueryTextFragment {
  LineIfThenElse(bool cond, String whenTrue, String whenFalse) {
    line = cond ? whenTrue : whenFalse;
  }
}

abstract class BindNameValuePair {
  Map<String, dynamic> bindNameValuePair = {};
}

/// Simple pair of name-value to bind they to query as variable.
/// Sample:
/// ```
/// Query(
///      [
///         Line('LET tag=@tag'),
///         ... list of query lines
///            (see Line(), LineIfThen(), IfThemElse() for details )
///      ],
///      bindVars: [ BindVar( 'tag', tag ) ],
///    ).toMap()
/// ```
class BindVar extends BindNameValuePair {
  BindVar(String name, value) {
    bindNameValuePair = {name: value};
  }
}

/// Pair of variable name-value to bind they into query with condition.
/// Sample:
/// ```
/// Query(
///      [
///         LineIfThen( tag != null, 'LET tag=@tag'),
///         // ... list of other query lines (see Line(), LineIfThen(), LineIfThenElse() for details )
///      ],
///      bindVars: [ BindVarIfThen( tag!=null, 'tag', tag ) ],
///    ).toMap()
/// ```
class BindVarIfThen extends BindNameValuePair {
  BindVarIfThen(bool condition, String nameIfTrue, valueIfTrue) {
    if (condition) {
      bindNameValuePair = {nameIfTrue: valueIfTrue};
    }
  }
}

/// Pair of variable name-value to bind they to query with condition.
/// Sample:
/// ```
/// Query(
///      [
///         LineIfThenElse(
///           tag != null, 'LET tag=@tag', 'LET other_name=@other_name'),
///         // ... list of query lines (see Line(), LineIfThen(), IfThemElse() for details )
///      ],
///      bindVars: [ BindVarIfThenElse(
///         tag!=null, 'tag', tag, 'other_name', other_value  ) ],
///    ).toMap()
/// ```
class BindVarIfThenElse extends BindNameValuePair {
  BindVarIfThenElse(bool condition, String nameIfTrue, valueIfTrue,
      String nameIfFalse, valueIfFalse) {
    if (condition) {
      bindNameValuePair = {nameIfTrue: valueIfTrue};
    } else {
      bindNameValuePair = {nameIfFalse: valueIfFalse};
    }
  }
}

/// Constructor for constructing query as object from
/// understandable arguments with condition support.
/// Query object do not call database.
/// Query object can be converting to [Map] via toMap() method
/// and used as argument to `query()` method 
/// of database client for sending.
/// For debug purpose query object can be printed as [String],
/// because it has toString() method.
/// Sample:
/// ```
/// var query = Query(
///      [
///        Line('LET tag=@tag'),
///        Line('FOR doc in docs'),
///        LineIfThen(
///           tag != null, 'FILTER doc.tags && POSITION(doc.tags, tag )'),
///        Line('SORT doc.datetime'),
///        LineIfThenElse(
///           without_content, 'return UNSET(doc,"content")', 'return doc'),
///      ],
///      bindVars: [BindVarIfThen(tag != null, 'tag', tag)],
///    ).toMap();
/// ```
class Query {

  // исходные структуры, задаются в конструкторах:
  List<QueryTextFragment> fragments = [];
  List<BindNameValuePair>? bindVars = [];

  // Результирующий собранный из всех bindVars:
  Map<String, dynamic> bindVarsMap = {};

  /// Returns binded vars from query object.
  Map<String, dynamic> get bindedVars => bindVarsMap;

  Query(this.fragments, {List<BindNameValuePair>? bindVars}) {
    this.bindVars = bindVars;
  }

  /// Like Query() but named constructor.
  Query.create( fragments, {List<BindNameValuePair>? bindVars} ){
    Query( fragments, bindVars: bindVars);
  }


  /// Returns query string from [Query] object.
  String queryString() =>
      fragments
      .map((f) => f.toString())
      .where((f) => f.isNotEmpty)
      .toList()
      .cast<String>()
      .join('\n');

  /// Returns created query object as [Map] structure, 
  /// which contains keys: `'query'` and `'bindVars'`.
  /// The `'query'` key value contains
  /// a query text, `queryString()` method result.
  /// The `'bindVars'` key value contains
  /// a structure with binded variables, `bindedVars` getter result.  
  Map<String,dynamic> toMap() {
    //соберем bindVars в один Map:
    for (var bv in bindVars!) {
      bindVarsMap.addAll(bv.bindNameValuePair);
    }

    var result = {
      'query': queryString(),
      'bindVars': bindedVars,
    };

    return result;
  }

  /// Human readable view of Query object.
  /// See `toMap()` method to get created query object as Map structure.
  @override
  String toString() => toMap().toString();

}


