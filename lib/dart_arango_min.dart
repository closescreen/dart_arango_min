library dart_arango_min;

import 'package:dart_arango_min/src/dart_arango_min_base.dart' as base;
import 'package:dart_arango_min/src/dart_arango_min_query.dart' as query;

export 'src/dart_arango_min_base.dart';
export 'src/dart_arango_min_query.dart';

class QueryWithClient extends query.Query {
  // ссылка на объект, создавший данный запрос.
  base.ArangoDBClient client;

  QueryWithClient(this.client, List<query.QueryTextFragment> fragments,
      {List<query.BindNameValuePair>? bindVars})
      : super.create(fragments, bindVars: bindVars);

  /// Adds one [QueryTextFragment] to inner store of
  /// query fragments in during step by step constructing Query.
  ///
  /// Sample:
  /// ```
  /// .addQueryTextFragment( Line( 'FOR u IN users RETURN u' ) )
  /// ```
  QueryWithClient addQueryTextFragment(query.QueryTextFragment fragment) {
    fragments.add(fragment);
    return this;
  }

  /// Adds one [Line] to inner store of query fragments.
  ///
  /// Sample:
  /// ```
  /// .addLine( 'FOR u IN users RETURN u' )
  /// ```
  QueryWithClient addLine(String line) =>
    addQueryTextFragment(query.Line(line));

  /// Adds one [LineIfThen] to inner store of query fragments.
  ///
  /// Sample:
  /// ```
  /// .addLineIfThen( user_name!=null, 'FILTER u.name==@user_name' )
  /// ```
  QueryWithClient addLineIfThen(bool cond, String line) =>
      addQueryTextFragment(query.LineIfThen(cond, line));

  /// Adds one pair of name-value binded to query
  /// Sample:
  /// ```
  /// .addBindNameValuePair( BindVar( 'user_name', 'Dmitriy' ) )
  /// ```
  QueryWithClient addBindNameValuePair(query.BindNameValuePair nameValuePair) {
    bindVars!.add(nameValuePair);
    return this;
  }

  /// Adds one pair of name-value binded to query
  /// Sample:
  /// ```
  /// .addBindVar( 'user_name', 'Dmitry' )
  /// ```
  QueryWithClient addBindVar(String name, value) =>
      addBindNameValuePair(query.BindVar(name, value));

  /// Adds one condition-depended pair of name-value binded to query
  /// Sample:
  /// ```
  /// .addBindVarIfThen( user_name!=null, 'user_name', user_name )
  /// ```
  QueryWithClient addBindVarIfThen(bool cond, String name, value) =>
      addBindNameValuePair(query.BindVarIfThen(cond, name, value));

  /// Adds one condition-depended pair of name-value binded to query
  /// Sample:
  /// ```
  /// .addBindVarIfThenElse(
  ///   user_name!=null, 'user_name', user_name, 'max_user_age', 30 )
  /// ```
  QueryWithClient addBindVarIfThenElse(
    bool cond, 
    String nameIfTrue, 
    valueIfTrue,
    nameIfFalse, 
    valuesIfFalse) =>
      addBindNameValuePair(query.BindVarIfThenElse(
          cond, nameIfTrue, valueIfTrue, nameIfFalse, valuesIfFalse));

  /// Calls `client.queryToList( this.toMap() )`
  /// where `client` is [ArangoDBClient] saved in `client` property.
  Future<List<dynamic>> runAndReturnFutureList() async =>
    await client.queryToList(toMap());

  /// Calls `client.queryToStream( this.toMap() )`
  /// where `client` is [ArangoDBClient] saved in `client` property.
  Stream<dynamic> runAndReturnStream() => client.queryToStream(toMap() as Map<String, Object>);
}

extension ArangoClientQuery on base.ArangoDBClient {
  /// Creates new [QueryWithClient] (with saved link to client) object
  /// for continue to constructing it later.
  QueryWithClient newQuery() {
    return QueryWithClient(this, []);
  }
}
